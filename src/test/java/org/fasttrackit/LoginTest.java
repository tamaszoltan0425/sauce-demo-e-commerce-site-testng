package org.fasttrackit;


import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import org.fasttrackit.config.Config;
import org.fasttrackit.dataprovider.AuthenticationDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

@Feature("Test cases to verify Login functionality")

public class LoginTest extends Config {
    LoginPage loginPage = new LoginPage();
    ProductsPage productsPage = new ProductsPage();

    @Step("Opening Login page")
    @BeforeMethod
    public void open_login_page() {
        loginPage.openLoginPage();
    }

    @Step("Refreshing window")
    @AfterMethod
    public void clean_up() {
        Selenide.refresh();
    }

    @Test(description = "User can open Swag Labs login page")
    @Story("Login page loads successfully")
    @Owner("Zoltan Tamas")
    @Description("Swag Labs login page can be loaded successfully")
    @Severity(SeverityLevel.BLOCKER)
    public void user_can_open_sauce_demo_login_page() {
        loginPage.getLoginPageTitle();
        assertEquals(loginPage.getLoginPageTitle(), "Swag Labs", "Page title should be Swag Labs");
    }

    @Story("Valid credentials, login button click")
    @Test(description = "Valid users using valid credentials can successfully login to Swag Labs site", dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class)
    @Severity(SeverityLevel.BLOCKER)
    @Owner("Zoltan Tamas")
    @Description("Using valid credentials, valid users can login by filling in login form and clicking on the login button")
    public void valid_user_can_successfully_login_with_valid_credentials(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        assertEquals(productsPage.getProductPageTitle(), "Products", "Products list must be visible for Standard User");
    }

    @Story("Invalid credentials, login button click")
    @Owner("Zoltan Tamas")
    @Description("Using invalid credentials, invalid users can not login by filling in login form and clicking on the login button")
    @Test(description = "Invalid users using invalid credentials can not login to Swag Labs site", dataProvider = "invalidCredentials", dataProviderClass = AuthenticationDataProvider.class)
    @Severity(SeverityLevel.CRITICAL)
    public void non_functional_authentication(InvalidAccount account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        String errorMsg = loginPage.getErrorMessage();
        assertTrue(loginPage.isErrorMessageVisible(), "Login with invalid credentials generates error message ");
        assertEquals(errorMsg, account.getErrorMessage());
    }

    @Story("Valid users, enter key hit")
    @Test(description = "Valid users using valid credentials can successfully login to Swag Labs site", dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class)
    @Severity(SeverityLevel.NORMAL)
    @Owner("Zoltan Tamas")
    @Description("Using valid credentials, valid users can login by filling in login form and hitting 'Enter' key")
    public void valid_user_can_successfully_login_with_valid_credentials_pressing_enter_key_not_clicking_login_button(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.enterKeyHit();
        assertEquals(productsPage.getProductPageTitle(), "Products", "Products list must be visible for Standard User");
    }

    @Story("Invalid users, enter key hit")
    @Test(dataProvider = "invalidCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "Using invalid credentials, invalid users can not login by filling in login form and hitting 'Enter' key")
    @Owner("Zoltan Tamas")
    @Severity(SeverityLevel.NORMAL)
    @Description("Using invalid credentials, invalid users can not login by filling in login form and hitting 'Enter' key")
    public void non_functional_authentication_with_enter_key_hit_instead_of_login_button_click(InvalidAccount account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.enterKeyHit();
        String errorMsg = loginPage.getErrorMessage();
        assertTrue(loginPage.isErrorMessageVisible(), "Login with invalid credentials generates error message ");
        assertEquals(errorMsg, account.getErrorMessage());
    }

}