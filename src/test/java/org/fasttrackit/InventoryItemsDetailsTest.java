package org.fasttrackit;


import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import org.fasttrackit.config.Config;
import org.fasttrackit.dataprovider.AuthenticationDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

@Feature("Test cases to verify valid user's interaction with the product's detailed page")

public class InventoryItemsDetailsTest extends Config {
    LoginPage loginPage = new LoginPage();
    Header header = new Header();
    InventoryItemDetailsPage inventoryItemDetailsPage = new InventoryItemDetailsPage();

    @Step("Opening Login page")
    @BeforeMethod
    public void open_login_page() {
        loginPage.openLoginPage();
    }

    @Step("Closing window")
    @AfterMethod
    public void clean_up() {
        Selenide.closeWebDriver();
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "Valid user can add a product to Cart from Products page")
    @Story("Inventory item's detail page and users interaction")
    @Owner("Zoltan Tamas")
    @Description("Valid user can add a product to Cart from Products page")
    @Severity(SeverityLevel.CRITICAL)
    public void valid_user_can_add_one_product_to_cart_from_products_page(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        InventoryItem inventoryItem = new InventoryItem("4");
        inventoryItem.clickAddToCartButton();
        assertEquals(header.getCartBadgeValue(), "1", "After adding one product to cart the badge should display 1");

    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "Add to cart button label should change to Remove after click")
    @Story("Inventory item's detail page and users interaction")
    @Owner("Zoltan Tamas")
    @Description("Add to cart button label should change to Remove after click")
    @Severity(SeverityLevel.CRITICAL)
    public void inventory_items_add_to_cart_button_reads_remove_after_valid_user_click(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        InventoryItem inventoryItem = new InventoryItem("4");
        inventoryItem.clickAddToCartButton();
        String buttonTextAfterClick = inventoryItem.getButtonText();
        assertEquals(buttonTextAfterClick, "Remove", "Add to cart button after 'click' should read 'Remove' ");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "Valid users can access product's detailed page")
    @Story("Inventory item's detail page and users interaction")
    @Owner("Zoltan Tamas")
    @Description("Valid users can access product's detailed page")
    @Severity(SeverityLevel.CRITICAL)
    public void logged_in_user_can_access_product_details_page(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        InventoryItem inventoryItem = new InventoryItem("4");
        inventoryItem.clickListingLink();
        assertEquals(inventoryItem.getListingTitle(), inventoryItemDetailsPage.getItemTitle(), "Title on card should match title in Product Details page");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "Item's prices should be the same on Products page and the item's detailed page")
    @Story("Inventory item's detail page and users interaction")
    @Owner("Zoltan Tamas")
    @Description("Item's prices should be the same on Products page and the item's detailed page")
    @Severity(SeverityLevel.NORMAL)
    public void logged_in_user_reads_same_price_on_products_page_and_product_details_page(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        InventoryItem inventoryItem = new InventoryItem("4");
        inventoryItem.clickListingLink();
        assertEquals(inventoryItemDetailsPage.getItemPrice(), 29.99, "Title on card should match title in Product Details page");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "Item's description should be the same on Products page and the item's detailed page")
    @Story("Inventory item's detail page and users interaction")
    @Owner("Zoltan Tamas")
    @Description("Item's description should be the same on Products page and the item's detailed page")
    @Severity(SeverityLevel.NORMAL)
    public void logged_in_user_reads_same_description_on_products_page_and_product_details_page(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        InventoryItem inventoryItem = new InventoryItem("4");
        inventoryItem.clickListingLink();
        assertEquals(inventoryItemDetailsPage.getItemDescription(), "carry.allTheThings() with the sleek, streamlined Sly Pack that melds uncompromising style with unequaled laptop and tablet protection.", "Title on card should match title in Product Details page");
    }


}
