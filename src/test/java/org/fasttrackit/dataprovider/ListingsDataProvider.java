package org.fasttrackit.dataprovider;

import org.fasttrackit.Account;
import org.fasttrackit.ListingsData;
import org.testng.annotations.DataProvider;

public class ListingsDataProvider {
    static ListingsData backpack = new ListingsData("4", "Sauce Labs Backpack");
    static ListingsData tShirt = new ListingsData("1", "Sauce Labs Bolt T-Shirt");
    static ListingsData onesie = new ListingsData("2", "Sauce Labs Onesie");
    static ListingsData bikeLight = new ListingsData("0", "Sauce Labs Bike Light");
    static ListingsData fleeceJAcket = new ListingsData("5", "Sauce Labs Fleece Jacket");
    static ListingsData redShirt = new ListingsData("3", "Test.allTheThings() T-Shirt (Red)");


    @DataProvider(name = "inventoryItems")
    public Object[][] getCredentials() {
        return new Object[][]{
                {backpack},
                {tShirt},
                {onesie},
                {bikeLight},
                {fleeceJAcket},
                {redShirt}


        };
    }
}
