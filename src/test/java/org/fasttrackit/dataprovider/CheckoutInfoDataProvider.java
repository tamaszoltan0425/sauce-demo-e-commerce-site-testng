package org.fasttrackit.dataprovider;

import org.fasttrackit.InvalidCheckoutInfo;
import org.fasttrackit.ValidCheckoutInfo;
import org.testng.annotations.DataProvider;

public class CheckoutInfoDataProvider {
    static ValidCheckoutInfo validCheckoutInfo1 = new ValidCheckoutInfo("John", "Doe", "123456");
    static ValidCheckoutInfo validCheckoutInfo2 = new ValidCheckoutInfo("Jane", "Doe", "123457");
    static ValidCheckoutInfo validCheckoutInfo3 = new ValidCheckoutInfo("Mac", "Donald", "023457");
    static ValidCheckoutInfo validCheckoutInfo4 = new ValidCheckoutInfo("Burger", "King", "023450");
    static InvalidCheckoutInfo emptyFirstName = new InvalidCheckoutInfo("", "Doe", "123456", "Error: First Name is required");
    static InvalidCheckoutInfo emptyLastName = new InvalidCheckoutInfo("John", "", "123456", "Error: Last Name is required");
    static InvalidCheckoutInfo emptyZipCode = new InvalidCheckoutInfo("John", "Doe", "", "Error: Postal Code is required");
    static InvalidCheckoutInfo emptyForm = new InvalidCheckoutInfo("", "", "", "Error: First Name is required");


    @DataProvider(name = "validCheckoutInfo")
    public Object[][] getCheckoutInfo() {
        return new Object[][]{
                {validCheckoutInfo1},
                {validCheckoutInfo2},
                {validCheckoutInfo3},
                {validCheckoutInfo4},
        };
    }

    @DataProvider(name = "invalidCheckoutInfo")
    public Object[][] getInvalidCheckoutInfo() {
        return new Object[][]{
                {emptyFirstName},
                {emptyLastName},
                {emptyZipCode},
                {emptyForm},
        };
    }

}
