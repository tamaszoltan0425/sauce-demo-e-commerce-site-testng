package org.fasttrackit.dataprovider;

import org.fasttrackit.ListingsData;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.List;

public class UsersAndProductsInteractionDataProvider {

    @DataProvider(name = "usersAndBackpack")
    public Object[][] getUsersAndBackpack() {

        return new Object[][]{
                {AuthenticationDataProvider.standardUser, ListingsDataProvider.backpack},
                {AuthenticationDataProvider.errorUser, ListingsDataProvider.backpack},
                {AuthenticationDataProvider.perfGlitchUser, ListingsDataProvider.backpack},
                {AuthenticationDataProvider.problemUser, ListingsDataProvider.backpack},
                {AuthenticationDataProvider.visualUser, ListingsDataProvider.backpack}

        };
    }

    @DataProvider(name = "usersAndBikeLight")
    public Object[][] getUsersAndBikeLight() {

        return new Object[][]{
                {AuthenticationDataProvider.standardUser, ListingsDataProvider.bikeLight},
                {AuthenticationDataProvider.errorUser, ListingsDataProvider.bikeLight},
                {AuthenticationDataProvider.perfGlitchUser, ListingsDataProvider.bikeLight},
                {AuthenticationDataProvider.problemUser, ListingsDataProvider.bikeLight},
                {AuthenticationDataProvider.visualUser, ListingsDataProvider.bikeLight}

        };
    }

    @DataProvider(name = "usersAndTshirt")
    public Object[][] getUsersAndTshirt() {

        return new Object[][]{
                {AuthenticationDataProvider.standardUser, ListingsDataProvider.tShirt},
                {AuthenticationDataProvider.errorUser, ListingsDataProvider.tShirt},
                {AuthenticationDataProvider.perfGlitchUser, ListingsDataProvider.tShirt},
                {AuthenticationDataProvider.problemUser, ListingsDataProvider.tShirt},
                {AuthenticationDataProvider.visualUser, ListingsDataProvider.tShirt}

        };
    }
    @DataProvider(name = "usersAndFleeceJacket")
    public Object[][] getUsersAndFleeceJacket() {

        return new Object[][]{
                {AuthenticationDataProvider.standardUser, ListingsDataProvider.fleeceJAcket},
                {AuthenticationDataProvider.errorUser, ListingsDataProvider.fleeceJAcket},
                {AuthenticationDataProvider.perfGlitchUser, ListingsDataProvider.fleeceJAcket},
                {AuthenticationDataProvider.problemUser, ListingsDataProvider.fleeceJAcket},
                {AuthenticationDataProvider.visualUser, ListingsDataProvider.fleeceJAcket}

        };
    }
    @DataProvider(name = "usersAndOnesie")
    public Object[][] getUsersAndOnesie() {

        return new Object[][]{
                {AuthenticationDataProvider.standardUser, ListingsDataProvider.onesie},
                {AuthenticationDataProvider.errorUser, ListingsDataProvider.onesie},
                {AuthenticationDataProvider.perfGlitchUser, ListingsDataProvider.onesie},
                {AuthenticationDataProvider.problemUser, ListingsDataProvider.onesie},
                {AuthenticationDataProvider.visualUser, ListingsDataProvider.onesie}

        };
    }
    @DataProvider(name = "usersAndRedShirt")
    public Object[][] getUsersAndRedShirt() {

        return new Object[][]{
                {AuthenticationDataProvider.standardUser, ListingsDataProvider.redShirt},
                {AuthenticationDataProvider.errorUser, ListingsDataProvider.redShirt},
                {AuthenticationDataProvider.perfGlitchUser, ListingsDataProvider.redShirt},
                {AuthenticationDataProvider.problemUser, ListingsDataProvider.redShirt},
                {AuthenticationDataProvider.visualUser, ListingsDataProvider.redShirt}

        };
    }
    @DataProvider(name = "usersAndProducts")
    public Object[][] getUsersAndProducts() {
        List<ListingsData> products = new ArrayList<>();
        products.add(ListingsDataProvider.backpack);
        products.add(ListingsDataProvider.onesie);
        products.add(ListingsDataProvider.bikeLight);
        return new Object[][] {
                {AuthenticationDataProvider.visualUser, products},
                {AuthenticationDataProvider.errorUser, products},
                {AuthenticationDataProvider.problemUser, products},
                {AuthenticationDataProvider.standardUser, products},
                {AuthenticationDataProvider.perfGlitchUser, products},
        };
    }
}
