package org.fasttrackit.dataprovider;

import org.fasttrackit.Account;
import org.fasttrackit.InvalidAccount;
import org.testng.annotations.DataProvider;

public class AuthenticationDataProvider {
    static Account standardUser = new Account("standard_user", "secret_sauce");
    static Account problemUser  = new Account("problem_user", "secret_sauce");
    static Account perfGlitchUser = new Account("performance_glitch_user", "secret_sauce");
    static Account errorUser = new Account("error_user", "secret_sauce");
    static Account visualUser = new Account("visual_user", "secret_sauce");

    public static Account getStandardUser() {
        return standardUser;
    }

    public static Account getProblemUser() {
        return problemUser;
    }

    public static Account getPerfGlitchUser() {
        return perfGlitchUser;
    }

    public static Account getErrorUser() {
        return errorUser;
    }

    public static Account getVisualUser() {
        return visualUser;
    }

    @DataProvider(name = "validCredentials")
    public Object[][] getCredentials() {

        return new Object[][] {
                {standardUser},
                {problemUser},
                {perfGlitchUser},
                {errorUser},
                {visualUser}

        };
    }
    @DataProvider(name = "invalidCredentials")
    public Object[][] getInvalidCredentials() {
        InvalidAccount locked_out_user = new InvalidAccount("locked_out_user", "secret_sauce", "Epic sadface: Sorry, this user has been locked out.");
        InvalidAccount emptyUsername = new InvalidAccount("", "secret_sauce", "Epic sadface: Username is required");
        InvalidAccount emptyPassword = new InvalidAccount("standard_user", "", "Epic sadface: Password is required");
        InvalidAccount unknownUser = new InvalidAccount("user123", "123456", "Epic sadface: Username and password do not match any user in this service");
        InvalidAccount wrongPassword = new InvalidAccount("standard_user", "123456", "Epic sadface: Username and password do not match any user in this service");
        InvalidAccount wrongUsername = new InvalidAccount("user123", "secret_sauce", "Epic sadface: Username and password do not match any user in this service");

        return new Object[][] {
                {locked_out_user},
                {emptyUsername},
                {emptyPassword},
                {unknownUser},
                {wrongPassword},
                {wrongUsername}
        };
    }
}
