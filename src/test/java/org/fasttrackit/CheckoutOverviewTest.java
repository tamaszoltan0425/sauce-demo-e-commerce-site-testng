package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import org.fasttrackit.config.Config;
import org.fasttrackit.dataprovider.CheckoutInfoDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static org.testng.Assert.*;

@Feature("Test cases to ensure the functionality of the Checkout Overview page")

public class CheckoutOverviewTest extends Config {
    private final String USERNAME = "standard_user";
    private final String PASSWORD = "secret_sauce";
    LoginPage loginPage = new LoginPage();
    ProductsPage productsPage = new ProductsPage();
    Header header = new Header();
    Cart cart = new Cart();
    CheckoutModal checkoutModal = new CheckoutModal();

    @Step("Logging in as Standard User")
    @BeforeMethod
    public void standard_user_login() {
        loginPage.openLoginPage();
        loginPage.typeUserName(USERNAME);
        loginPage.typePassword(PASSWORD);
        loginPage.clickLoginButton();
    }

    @Step("Closing window")
    @AfterMethod
    public void clean_up() {
        Selenide.closeWebDriver();
    }

    @Test(dataProvider = "validCheckoutInfo", dataProviderClass = CheckoutInfoDataProvider.class, description = "Item prices added to Cart by standard user add up to subtotal")
    @Story("Standard user checkout experience")
    @Owner("Zoltan Tamas")
    @Description("Item prices added to Cart by standard user add up to subtotal")
    @Severity(SeverityLevel.CRITICAL)
    public void items_prices_add_up_to_subtotal(ValidCheckoutInfo validCheckoutInfo) {
        productsPage.openProductsPage();
        InventoryItem inventoryItem = new InventoryItem("4");
        inventoryItem.clickAddToCartButton();
        inventoryItem = new InventoryItem("0");
        inventoryItem.clickAddToCartButton();
        header.clickOnTheCart();
        cart.clickCheckout();
        checkoutModal.typeInFirstName(validCheckoutInfo.getFirstName());
        checkoutModal.typeInLastName(validCheckoutInfo.getLastName());
        checkoutModal.typeInZip(validCheckoutInfo.getZipCode());
        checkoutModal.clickContinueCheckout();
        CheckoutOverview checkoutOverview = new CheckoutOverview("4");
        double price1 = checkoutOverview.getCartItemPrice();
        checkoutOverview = new CheckoutOverview("0");
        double price2 = checkoutOverview.getCartItemPrice();
        assertEquals(checkoutOverview.getItemsSubTotal(), price1 + price2, "Items prices should add up to total before tax");

    }

    @Test(dataProvider = "validCheckoutInfo", dataProviderClass = CheckoutInfoDataProvider.class, description = "Item prices total add up to subtotal plus tax")
    @Story("Standard user checkout experience")
    @Owner("Zoltan Tamas")
    @Description("Item prices total add up to subtotal plus tax")
    @Severity(SeverityLevel.CRITICAL)
    public void total_price_of_items_is_equal_to_subtotal_plus_tax(ValidCheckoutInfo validCheckoutInfo) {
        productsPage.openProductsPage();
        InventoryItem inventoryItem = new InventoryItem("4");
        inventoryItem.clickAddToCartButton();
        inventoryItem = new InventoryItem("0");
        inventoryItem.clickAddToCartButton();
        header.clickOnTheCart();
        cart.clickCheckout();
        checkoutModal.typeInFirstName(validCheckoutInfo.getFirstName());
        checkoutModal.typeInLastName(validCheckoutInfo.getLastName());
        checkoutModal.typeInZip(validCheckoutInfo.getZipCode());
        checkoutModal.clickContinueCheckout();
        CheckoutOverview checkoutOverview = new CheckoutOverview("4");
        double price1 = checkoutOverview.getCartItemPrice();
        checkoutOverview = new CheckoutOverview("0");
        double price2 = checkoutOverview.getCartItemPrice();
        assertEquals(checkoutOverview.getTotalPrice(), price1 + price2 + checkoutOverview.getItemsTax(), "Tax plus Sub Total should equal Total");
    }

    @Test(dataProvider = "validCheckoutInfo", dataProviderClass = CheckoutInfoDataProvider.class, description = "Standard user can successfully checkout by clicking finish")
    @Story("Standard user checkout experience")
    @Owner("Zoltan Tamas")
    @Description("Standard user can successfully checkout by clicking finish")
    @Severity(SeverityLevel.CRITICAL)
    public void standard_user_can_successfully_checkout_by_clicking_finish(ValidCheckoutInfo validCheckoutInfo) {
        productsPage.openProductsPage();
        InventoryItem inventoryItem = new InventoryItem("4");
        inventoryItem.clickAddToCartButton();
        inventoryItem = new InventoryItem("0");
        inventoryItem.clickAddToCartButton();
        header.clickOnTheCart();
        cart.clickCheckout();
        checkoutModal.typeInFirstName(validCheckoutInfo.getFirstName());
        checkoutModal.typeInLastName(validCheckoutInfo.getLastName());
        checkoutModal.typeInZip(validCheckoutInfo.getZipCode());
        checkoutModal.clickContinueCheckout();
        CheckoutOverview checkoutOverview = new CheckoutOverview("4");
        checkoutOverview.clickFinish();
        assertEquals(checkoutOverview.getSuccessMessage(), "Thank you for your order!", "Standard user reads success message after checkout");

    }

}