package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import org.fasttrackit.config.Config;
import org.fasttrackit.dataprovider.AuthenticationDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class SortingTest extends Config {
    LoginPage loginPage = new LoginPage();
    ProductsPage productsPage = new ProductsPage();
    Header header = new Header();

    @Step("Opening Login page")
    @BeforeMethod
    public void open_login_page() {
        loginPage.openLoginPage();
    }

    @Step("Refreshing page")
    @AfterMethod
    public void clean_up() {
        Selenide.refresh();
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "After successful login standard user can sort products alphabetically increasing")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Zoltan Tamas")
    @Description("After successful login standard user can sort products alphabetically increasing")
    @Story("Logged in user items Sorting on Swag Labs products page")
    public void valid_user_can_sort_inventory_items_alphabetically_increasing(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        productsPage.setSortByAtoZ();
        assertTrue(productsPage.inventoryAtoZ(), "Inventory items should be displayed alphabetically increasing");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "After successful login standard user can sort products alphabetically decreasing")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Zoltan Tamas")
    @Description("After successful login standard user can sort products alphabetically decreasing")
    @Story("Logged in user items Sorting on Swag Labs products page")
    public void valid_user_can_sort_inventory_items_alphabetically_decreasing(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        productsPage.setSortByZtoA();
        assertTrue(productsPage.inventoryZtoA(), "Inventory items should be sorted alphabetically decreasing");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "After successful login and initial alphabetically decreasing sorting, valid user can reset the sort to alphabetically increasing")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Zoltan Tamas")
    @Description("After successful login and initial alphabetically decreasing sorting, valid user can reset the sort to alphabetically increasing")
    @Story("Logged in user items Sorting on Swag Labs products page")
    public void clicking_the_reset_app_state_button_afterZtoA_sorting_sorts_inventory_items_alphabetically_increasing(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        productsPage.setSortByZtoA();
        header.openPageMenu();
        assertTrue(productsPage.inventoryZtoA(), "Inventory items should be displayed alphabetically decreasing");
        productsPage.clickToResetState();
        assertTrue(productsPage.inventoryAtoZ(), "Inventory items should be displayed in default mode (A - Z)");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "After successful login valid user can sort products by price increasing")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Zoltan Tamas")
    @Description("After successful login valid user can sort products by price increasing")
    @Story("Logged in user interaction with Swag Labs products page")
    public void logged_in_user_can_sort_inventory_items_by_price_increasing(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        productsPage.setSortLotoHi();
        assertTrue(productsPage.inventoryPricesAscending(), "Products should be sorted by price increasing");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "After successful login and initial price increasing sorting, valid user can reset the sort to alphabetically increasing")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Zoltan Tamas")
    @Description("After successful login and initial price increasing sorting, valid user can reset the sort to alphabetically increasing")
    @Story("Logged in user interaction with Swag Labs products page")
    public void clicking_the_reset_app_state_button_afterPriceIncreasing_sorting_sorts_inventory_items_alphabetically_increasing(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        productsPage.setSortLotoHi();
        header.openPageMenu();
        assertTrue(productsPage.inventoryPricesAscending(), "Inventory items should be displayed by their price increasing");
        productsPage.clickToResetState();
        assertTrue(productsPage.inventoryAtoZ(), "Inventory items should be displayed in default mode (A - Z)");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "After successful login valid user can sort by price decreasing")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Zoltan Tamas")
    @Description("After successful login valid user can sort by price decreasing")
    @Story("Logged in user interaction with Swag Labs products page")
    public void standard_user_can_sort_inventory_items_by_price_descending(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        productsPage.setSortHitoLo();
        assertTrue(productsPage.inventoryPricesDescending(), "Products should be sorted by price descending");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "After successful login and initial price decreasing sorting, valid user can reset the sort to alphabetically increasing")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Zoltan Tamas")
    @Description("After successful login and initial price decreasing sorting, valid user can reset the sort to alphabetically increasing")
    @Story("Logged in user interaction with Swag Labs products page")
    public void clicking_the_reset_app_state_button_afterPriceDecreasing_sorting_sorts_inventory_items_alphabetically_increasing(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        productsPage.setSortHitoLo();
        header.openPageMenu();
        assertTrue(productsPage.inventoryPricesDescending(), "Inventory items should be displayed by their price decreasing");
        productsPage.clickToResetState();
        assertTrue(productsPage.inventoryAtoZ(), "Inventory items should be displayed in default mode (A - Z)");
    }
}
