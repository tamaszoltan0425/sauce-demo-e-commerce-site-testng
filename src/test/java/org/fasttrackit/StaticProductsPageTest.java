package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import org.fasttrackit.config.Config;
import org.fasttrackit.dataprovider.AuthenticationDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class StaticProductsPageTest extends Config {
    LoginPage loginPage = new LoginPage();
    ProductsPage productsPage = new ProductsPage();
    Header header = new Header();
    Footer footer = new Footer();

    @Step("Opening Login page")
    @BeforeMethod
    public void open_login_page() {
        loginPage.openLoginPage();
    }

    @Step("Refreshing page")
    @AfterMethod
    public void clean_up() {
        Selenide.refresh();
    }


    @Test(description = "After successful login six inventory items should be visible", dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class)
    @Severity(SeverityLevel.NORMAL)
    @Owner("Zoltan Tamas")
    @Description("After successful login six inventory items should be visible")
    @Story("Logged in user visual experience on Swag Labs")
    public void all_products_should_be_visible(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        assertEquals(productsPage.getNumberOfElements(), 6, "Should be six inventory items.");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "After successful login Swag Labs title should be visible")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Zoltan Tamas")
    @Description("After successful login Swag Labs title should be visible")
    @Story("Logged in user visual experience on Swag Labs")
    public void page_title_should_be_visible(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        assertTrue(header.siteNameIsDisplayed(), "Should be displayed in Header");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "After successful login burger button should be visible")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Zoltan Tamas")
    @Description("After successful login burger button should be visible")
    @Story("Logged in user visual experience on Swag Labs")
    public void burger_menu_button_is_displayed(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        assertTrue(header.burgerMenuButtonIsDisplayed(), "Should be displayed in Header");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "After successful login cart icon should be visible")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Zoltan Tamas")
    @Description("After successful login cart icon should be visible")
    @Story("Logged in user visual experience on Swag Labs")
    public void cart_icon_in_header_is_displayed(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        assertTrue(header.cartIconIsDisplayed(), "Should be displayed in Header");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "After successful login 'Sort by' menu should be visible")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Zoltan Tamas")
    @Description("After successful login 'Sort by' menu should be visible")
    @Story("Logged in user visual experience on Swag Labs")
    public void sort_by_list_is_displayed(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        assertTrue(productsPage.sortByContainerisDisplayed(), "Should be displayed");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "After successful login Twitter logo should be visible")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Zoltan Tamas")
    @Description("After successful login Twitter logo should be visible")
    @Story("Logged in user visual experience on Swag Labs")
    public void twitter_logo_in_footer_is_displayed(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        assertTrue(footer.twitterLogoVisible(), "Twitter logo should be displayed in the footer");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "After successful login Facebook logo should be visible")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Zoltan Tamas")
    @Description("After successful login Facebook logo should be visible")
    @Story("Logged in user visual experience on Swag Labs")
    public void facebook_logo_in_footer_is_displayed(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        assertTrue(footer.facebookLogoVisible(), "Facebook logo should be displayed in the footer");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "After successful login Linkedin logo should be visible")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Zoltan Tamas")
    @Description("After successful login Linkedin logo should be visible")
    @Story("Logged in user visual experience on Swag Labs")
    public void linkedin_logo_in_footer_is_displayed(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        assertTrue(footer.linkedinLogoVisible(), "Linkedin logo should be displayed in the footer");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "After successful login copyright should be visible")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Zoltan Tamas")
    @Description("After successful login copyright should be visible")
    @Story("Logged in user visual experience on Swag Labs")
    public void copyright_text_in_footer_is_displayed(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        assertTrue(footer.copyrightIsVisible(), "Copyright text should be displayed in footer");
    }
}
