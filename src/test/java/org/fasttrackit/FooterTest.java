package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.*;
import org.fasttrackit.config.Config;
import org.fasttrackit.dataprovider.AuthenticationDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Feature("Test cases to verify the functionality of links in Footer")

public class FooterTest extends Config {
    LoginPage loginPage = new LoginPage();
    Footer footer = new Footer();

    @Step("Opening Login page")
    @BeforeMethod
    public void open_login_page() {
        loginPage.openLoginPage();
    }

    @Step("Closing window")
    @AfterMethod
    public void clean_up() {
        Selenide.closeWebDriver();
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "After successful login, user can navigate to Linkedin")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Zoltan Tamas")
    @Description("After successful login, user can navigate to Linkedin")
    @Story("Logged in user interaction with footer links")
    public void valid_user_can_navigate_to_swaglabs_linkedin_page_from_footer(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        footer.linkedinClick();
        Selenide.switchTo().window(1);
        String newTabURL = WebDriverRunner.getWebDriver().getCurrentUrl();
        assertEquals(newTabURL, "https://www.linkedin.com/company/sauce-labs/", "Standard user should be on Swag Labs LinkedIn page");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "After successful login, user can navigate to Facebook")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Zoltan Tamas")
    @Description("After successful login, user can navigate to Facebook")
    @Story("Logged in user interaction with footer links")
    public void valid_user_can_navigate_to_swaglabs_facebook_page_from_footer(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        footer.facebookClick();
        Selenide.switchTo().window(1);
        String newTabURL = WebDriverRunner.getWebDriver().getCurrentUrl();
        assertEquals(newTabURL, "https://www.facebook.com/saucelabs", "Standard user should be on Swag Labs Facebook page");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "After successful login, user can navigate to Twitter")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Zoltan Tamas")
    @Description("After successful login, user can navigate to Twitter")
    @Story("Logged in user interaction with footer links")
    public void valid_user_can_navigate_to_swaglabs_twitter_page_from_footer(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        footer.twitterClick();
        Selenide.switchTo().window(1);
        String newTabURL = WebDriverRunner.getWebDriver().getCurrentUrl();
        assertEquals(newTabURL, "https://twitter.com/saucelabs", "Standard user should be on Swag Labs Twitter page");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "After successful login, user can navigate to Twitter")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Zoltan Tamas")
    @Description("After successful login, user can navigate to Twitter")
    @Story("Logged in user interaction with footer links")
    public void valid_user_can_read_copyright_text_from_footer(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        assertEquals(footer.copyRightText(), "© 2023 Sauce Labs. All Rights Reserved. Terms of Service | Privacy Policy", "Standard user should be on Swag Labs Twitter page");
    }
}
