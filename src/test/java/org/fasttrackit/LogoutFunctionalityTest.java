package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.*;
import org.fasttrackit.config.Config;
import org.fasttrackit.dataprovider.AuthenticationDataProvider;
import org.testng.annotations.*;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static org.testng.Assert.*;

@Feature("Test cases to verify Logout functionality")

public class LogoutFunctionalityTest extends Config {
    LoginPage loginPage = new LoginPage();
    ProductsPage productsPage = new ProductsPage();
    Header header = new Header();

    @Step("Opening Login page")
    @BeforeMethod
    public void open_login_page() {
        loginPage.openLoginPage();
    }

    @Step("Refreshing window")
    @AfterMethod
    public void clean_up() {
        Selenide.refresh();
    }

    @Story("Successfully logged in users can logout")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Zoltan Tamas")
    @Description("Using valid credentials, valid users can login by filling in login form and clicking on the login button")
    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "Using valid credentials, valid users can login by filling in login form and clicking on the login button")
    public void successfully_logged_in_users_can_logout(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        header.openPageMenu();
        productsPage.logOutCurrentUser();
        assertEquals(WebDriverRunner.getWebDriver().getCurrentUrl(), "https://www.saucedemo.com/", "URLs must be www.saucedemo.com");
    }


}
