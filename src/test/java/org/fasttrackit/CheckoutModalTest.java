package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.*;
import org.fasttrackit.config.Config;
import org.fasttrackit.dataprovider.CheckoutInfoDataProvider;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

@Feature("Test cases to ensure the functionality of the Checkout modal")
public class CheckoutModalTest extends Config {
    private final String USERNAME = "standard_user";
    private final String PASSWORD = "secret_sauce";
    LoginPage loginPage = new LoginPage();
    ProductsPage productsPage = new ProductsPage();
    Header header = new Header();
    Cart cart = new Cart();
    CheckoutModal checkoutModal = new CheckoutModal();

    @Step("Logging in as Standard User")
    @BeforeClass
    public void standard_user_login() {
        loginPage.openLoginPage();
        loginPage.typeUserName(USERNAME);
        loginPage.typePassword(PASSWORD);
        loginPage.clickLoginButton();
    }

    @Step("Refreshing page")
    @AfterMethod
    public void close_session() {
        Selenide.refresh();
    }

    @Step("Closing window")
    @AfterClass
    public void close_window() {
        Selenide.closeWebDriver();
    }

    @Test(dataProvider = "validCheckoutInfo", dataProviderClass = CheckoutInfoDataProvider.class, description = "First name input field should only accept valid, alphanumerical input")
    @Story("Standard user checkout experience")
    @Owner("Zoltan Tamas")
    @Description("First name input field should only accept valid, alphanumerical input")
    @Severity(SeverityLevel.NORMAL)
    public void first_name_field_should_accept_valid_checkout_info(ValidCheckoutInfo validCheckoutInfo) {
        productsPage.openProductsPage();
        InventoryItem inventoryItem = new InventoryItem("4");
        inventoryItem.clickAddToCartButton();
        header.clickOnTheCart();
        cart.clickCheckout();
        checkoutModal.typeInFirstName(validCheckoutInfo.getFirstName());
        assertTrue(checkoutModal.isFirstNameValid(), "First name should only contain alphabetical characters");
    }

    @Test(dataProvider = "validCheckoutInfo", dataProviderClass = CheckoutInfoDataProvider.class, description = "Last name input field should only accept valid, alphanumerical input")
    @Story("Standard user checkout experience")
    @Owner("Zoltan Tamas")
    @Description("Last name input field should only accept valid, alphanumerical input")
    @Severity(SeverityLevel.NORMAL)
    public void last_name_field_should_only_accept_alphabetical_characters(CheckoutInfo validCheckoutInfo) {
        productsPage.openProductsPage();
        InventoryItem inventoryItem = new InventoryItem("4");
        inventoryItem.clickAddToCartButton();
        header.clickOnTheCart();
        cart.clickCheckout();
        checkoutModal.typeInLastName(validCheckoutInfo.getLastName());
        assertTrue(checkoutModal.isLastNameValid(), "Last name should only contain alphabetical characters");
    }

    @Test(dataProvider = "validCheckoutInfo", dataProviderClass = CheckoutInfoDataProvider.class, description = "Postal code input field should only valid, numerical input, six numerical characters")
    @Story("Standard user checkout experience")
    @Owner("Zoltan Tamas")
    @Description("Postal code input field should only valid, numerical input, six numerical characters")
    @Severity(SeverityLevel.NORMAL)
    public void zip_input_field_should_only_accept_six_numerical_characters(CheckoutInfo validCheckoutInfo) {
        productsPage.openProductsPage();
        InventoryItem inventoryItem = new InventoryItem("4");
        inventoryItem.clickAddToCartButton();
        header.clickOnTheCart();
        cart.clickCheckout();
        checkoutModal.typeInZip(validCheckoutInfo.getZipCode());
        assertTrue(checkoutModal.isZipCodeValid(), "Zip code should contain six numerical characters only");
    }

    @Test(description = "Standard user can navigate back to cart page")
    @Story("Standard user checkout experience")
    @Owner("Zoltan Tamas")
    @Description("Standard user can navigate back to cart page ")
    @Severity(SeverityLevel.NORMAL)
    public void standard_user_can_go_back_to_cart_by_clicking_cancel() {
        productsPage.openProductsPage();
        InventoryItem inventoryItem = new InventoryItem("4");
        inventoryItem.clickAddToCartButton();
        header.clickOnTheCart();
        cart.clickCheckout();
        checkoutModal.clickCancel();
        assertEquals(cart.getCartPageTitle(), "Your Cart", "Standard user should be on cart page");
    }

    @Test(dataProvider = "validCheckoutInfo", dataProviderClass = CheckoutInfoDataProvider.class, description = "Standard user can continue checking out by submitting valid checkout data ")
    @Story("Standard user checkout experience")
    @Owner("Zoltan Tamas")
    @Description("Standard user can continue checking out by submitting valid checkout data ")
    @Severity(SeverityLevel.NORMAL)
    public void standard_user_can_continue_checkout_by_submitting_valid_checkout_data(CheckoutInfo validCheckoutInfo) {
        productsPage.openProductsPage();
        InventoryItem inventoryItem = new InventoryItem("4");
        inventoryItem.clickAddToCartButton();
        header.clickOnTheCart();
        cart.clickCheckout();
        checkoutModal.typeInFirstName(validCheckoutInfo.getFirstName());
        checkoutModal.typeInLastName(validCheckoutInfo.getLastName());
        checkoutModal.typeInZip(validCheckoutInfo.getZipCode());
        checkoutModal.clickContinueCheckout();
        assertEquals(WebDriverRunner.getWebDriver().getCurrentUrl(), "https://www.saucedemo.com/checkout-step-two.html", "After successfully submitting valid data, user should be on Overview page ");
    }

    @Test(dataProvider = "invalidCheckoutInfo", dataProviderClass = CheckoutInfoDataProvider.class, description = "Leaving the input fields blank will generate error message")
    @Story("Standard user checkout experience")
    @Owner("Zoltan Tamas")
    @Description("Leaving the input fields blank will generate error message")
    @Severity(SeverityLevel.NORMAL)
    public void not_filling_in_checkout_form_fields_generates_error_message(InvalidCheckoutInfo invalidCheckoutInfo) {
        productsPage.openProductsPage();
        InventoryItem inventoryItem = new InventoryItem("4");
        inventoryItem.clickAddToCartButton();
        header.clickOnTheCart();
        cart.clickCheckout();
        checkoutModal.typeInFirstName(invalidCheckoutInfo.getFirstName());
        checkoutModal.typeInLastName(invalidCheckoutInfo.getLastName());
        checkoutModal.typeInZip(invalidCheckoutInfo.getZipCode());
        checkoutModal.clickContinueCheckout();
        String errorMessage = checkoutModal.getErrorMessage();
        assertTrue(checkoutModal.isCheckoutErrorMessageVisible());
        assertEquals(errorMessage, invalidCheckoutInfo.getCheckOutErrorMessage());

    }


}