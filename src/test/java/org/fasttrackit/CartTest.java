package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.*;
import org.fasttrackit.config.Config;
import org.fasttrackit.dataprovider.AuthenticationDataProvider;
import org.fasttrackit.dataprovider.ListingsDataProvider;
import org.fasttrackit.dataprovider.UsersAndProductsInteractionDataProvider;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.*;
import static org.testng.Assert.*;

@Feature("Test cases to verify cart management functionality")

public class CartTest extends Config {
    LoginPage loginPage = new LoginPage();
    ProductsPage productsPage = new ProductsPage();
    Header header = new Header();
    Cart cart = new Cart();

    @Step("Opening Login page")
    @BeforeMethod
    public void user_login() {
        loginPage.openLoginPage();
    }

    @Step("Closing window")
    @AfterMethod
    public void clean_up() {
        WebDriverRunner.closeWebDriver();
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "Valid user can access cart page be clicking on Cart icon in Header")
    @Story("Cart page can be accessed successfully")
    @Owner("Zoltan Tamas")
    @Description("Valid user can access cart page be clicking on Cart icon in Header")
    @Severity(SeverityLevel.CRITICAL)
    public void valid_user_can_access_cart_page_from_products_page(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        header.clickOnTheCart();
        cart.getCartPageTitle();
        assertEquals(cart.getCartPageTitle(), "Your Cart", "Your Cart page title should be displayed");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "The Checkout button should be disabled while the Cart is empty")
    @Story("Cart page user interactions")
    @Owner("Zoltan Tamas")
    @Description("The Checkout button should be disabled while the Cart is empty")
    @Severity(SeverityLevel.NORMAL)
    public void checkout_button_must_be_disabled_while_cart_is_empty(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        header.clickOnTheCart();
        assertTrue(cart.cartIsEmpty(), "There are no items added to cart.");
        assertFalse(cart.checkoutButtonIsClickable(), "Checkout button should not be enabled.");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class, description = "Valid user can navigate back to the Products page from Cart")
    @Story("Cart page user interactions")
    @Owner("Zoltan Tamas")
    @Description("Valid user can navigate back to the Products page from Cart")
    @Severity(SeverityLevel.NORMAL)
    public void valid_user_can_navigate_to_products_page_from_cart_page_with_continue_shopping_button(Account account) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        header.clickOnTheCart();
        cart.clickContinueShopping();
        assertEquals(productsPage.getProductPageTitle(), "Products", "User should land on Products page.");
    }

    @Test(dataProvider = "usersAndBackpack", dataProviderClass = UsersAndProductsInteractionDataProvider.class, description = "Item added to Cart by valid user should be same item shown in Cart")
    @Story("Cart page user interactions")
    @Owner("Zoltan Tamas")
    @Description("Item added to Cart by valid user should be same item shown in Cart")
    @Severity(SeverityLevel.CRITICAL)
    public void item_added_to_cart_by_valid_user_is_same_item_shown_in_cart(Account account, ListingsData listingsData) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        InventoryItem inventoryItem = new InventoryItem(listingsData.getId());
        inventoryItem.clickAddToCartButton();
        header.clickOnTheCart();
        CartItem cartItem = new CartItem(listingsData.getId());
        assertEquals(cartItem.getCartItemTitle(), inventoryItem.getListingTitle(), "Should match");
    }

    @Test(dataProvider = "usersAndBackpack", dataProviderClass = UsersAndProductsInteractionDataProvider.class, description = "User can remove the one item added to Cart")
    @Story("Cart page user interactions")
    @Owner("Zoltan Tamas")
    @Description("User can remove the one item added to Cart")
    @Severity(SeverityLevel.CRITICAL)
    public void valid_user_can_remove_item_added_to_cart(Account account, ListingsData listingsData) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        InventoryItem inventoryItem = new InventoryItem(listingsData.getId());
        inventoryItem.clickAddToCartButton();
        header.clickOnTheCart();
        CartItem cartItem = new CartItem(listingsData.getId());
        cartItem.clickToRemoveItem();
        assertEquals(cart.getCartSize(), 0, "Cart size should be null");

    }

    @Test(dataProvider = "usersAndProducts", dataProviderClass = UsersAndProductsInteractionDataProvider.class, description = "Items added to the Cart by valid users are the same items shown in Cart")
    @Story("Cart page user interactions")
    @Owner("Zoltan Tamas")
    @Description("Items added to the Cart by valid users are the same items shown in Cart")
    @Severity(SeverityLevel.CRITICAL)
    public void items_added_to_cart_by_valid_user_are_same_items_shown_in_cart(Account account, List<ListingsData> listingsData) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        InventoryItem inventoryItem = new InventoryItem(listingsData.get(0).getId());
        String inv1 = inventoryItem.getListingTitle();
        inventoryItem.clickAddToCartButton();
        inventoryItem = new InventoryItem(listingsData.get(1).getId());
        String inv2 = inventoryItem.getListingTitle();
        inventoryItem.clickAddToCartButton();
        inventoryItem = new InventoryItem(listingsData.get(2).getId());
        String inv3 = inventoryItem.getListingTitle();
        inventoryItem.clickAddToCartButton();
        header.clickOnTheCart();
        CartItem first = new CartItem(listingsData.get(0).getId());
        CartItem second = new CartItem(listingsData.get(1).getId());
        CartItem third = new CartItem(listingsData.get(2).getId());
        assertEquals(first.getCartItemTitle(), inv1, "Should match");
        assertEquals(second.getCartItemTitle(), inv2, "should match");
        assertEquals(third.getCartItemTitle(), inv3, "should match");
    }

    @Test(dataProvider = "usersAndProducts", dataProviderClass = UsersAndProductsInteractionDataProvider.class, description = "User can remove the items added to Cart")
    @Story("Cart page user interactions")
    @Owner("Zoltan Tamas")
    @Description("User can remove the items added to Cart")
    @Severity(SeverityLevel.CRITICAL)
    public void valid_user_can_remove_items_added_to_cart(Account account, List<ListingsData> listingsData) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        InventoryItem inventoryItem = new InventoryItem(listingsData.get(0).getId());
        inventoryItem.clickAddToCartButton();
        inventoryItem = new InventoryItem(listingsData.get(1).getId());
        inventoryItem.clickAddToCartButton();
        inventoryItem = new InventoryItem(listingsData.get(2).getId());
        inventoryItem.clickAddToCartButton();
        header.clickOnTheCart();
        CartItem cartItem = new CartItem(listingsData.get(0).getId());
        cartItem.clickToRemoveItem();
        cartItem = new CartItem(listingsData.get(1).getId());
        cartItem.clickToRemoveItem();
        cartItem = new CartItem(listingsData.get(2).getId());
        cartItem.clickToRemoveItem();
        assertEquals(cart.getCartSize(), 0, "Cart size should be null");


    }


}