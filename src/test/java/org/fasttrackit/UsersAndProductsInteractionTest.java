package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.*;
import org.fasttrackit.config.Config;
import org.fasttrackit.dataprovider.UsersAndProductsInteractionDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("Test cases to verify the interaction between logged users and products")
public class UsersAndProductsInteractionTest extends Config {
    LoginPage loginPage = new LoginPage();
    ProductsPage productsPage = new ProductsPage();
    Header header = new Header();


    @Step("Opening Login page")
    @BeforeMethod
    public void setup() {
        loginPage.openLoginPage();
    }

    @Step("Closing window")
    @AfterMethod
    public void cleanup() {
        Selenide.closeWebDriver();
    }

    @Test(dataProvider = "usersAndBackpack", dataProviderClass = UsersAndProductsInteractionDataProvider.class, description = "Valid users can add 'Sauce Labs Backpack' to the cart")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Zoltan Tamas")
    @Description("Valid users can add 'Sauce Labs Backpack' to the cart")
    @Story("Valid user interaction with 'Sauce Labs Backpack' inventory item")
    public void successfully_logged_in_users_can_add_backpack_item_to_cart(Account account, ListingsData listingsData) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        productsPage.openProductsPage();
        InventoryItem inventoryItem = new InventoryItem(listingsData.getId());
        inventoryItem.clickAddToCartButton();
        header.clickOnTheCart();
        CartItem cartItem = new CartItem(listingsData.getId());
        assertEquals(cartItem.getCartItemTitle(), inventoryItem.getListingTitle(), "Product listing title and cart item' s title should be the same");
    }

    @Test(dataProvider = "usersAndBikeLight", dataProviderClass = UsersAndProductsInteractionDataProvider.class, description = "Valid users can add 'Sauce Labs Bike Light' to the cart")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Zoltan Tamas")
    @Description("Valid users can add 'Sauce Labs Bike Light' to the cart")
    @Story("Valid user interaction with 'Sauce Labs Bike Light' inventory item")
    public void successfully_logged_in_users_can_add_bike_light_item_to_cart(Account account, ListingsData listingsData) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        productsPage.openProductsPage();
        InventoryItem inventoryItem = new InventoryItem(listingsData.getId());
        inventoryItem.clickAddToCartButton();
        header.clickOnTheCart();
        CartItem cartItem = new CartItem(listingsData.getId());
        assertEquals(cartItem.getCartItemTitle(), inventoryItem.getListingTitle(), "Product listing title and cart item' s title should be the same");
    }

    @Test(dataProvider = "usersAndOnesie", dataProviderClass = UsersAndProductsInteractionDataProvider.class, description = "Valid users can add 'Sauce Labs Onesie' to the cart")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Zoltan Tamas")
    @Description("Valid users can add 'Sauce Labs Onesie' to the cart")
    @Story("Valid user interaction with 'Sauce Labs Onesie' inventory item")
    public void successfully_logged_in_users_can_add_onesie_item_to_cart(Account account, ListingsData listingsData) {
        loginPage.typeUserName(account.getUser());
        loginPage.typePassword(account.getPassword());
        loginPage.clickLoginButton();
        productsPage.openProductsPage();
        InventoryItem inventoryItem = new InventoryItem(listingsData.getId());
        inventoryItem.clickAddToCartButton();
        header.clickOnTheCart();
        CartItem cartItem = new CartItem(listingsData.getId());
        assertEquals(cartItem.getCartItemTitle(), inventoryItem.getListingTitle(), "Product listing title and cart item' s title should be the same");
    }


}
