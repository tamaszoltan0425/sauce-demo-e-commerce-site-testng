package org.fasttrackit;

public class ValidCheckoutInfo extends CheckoutInfo{

    public ValidCheckoutInfo(String firstName, String lastName, String zipCode) {
        super(firstName, lastName, zipCode);
    }
}
