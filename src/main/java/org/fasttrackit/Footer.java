package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.WebDriverRunner.url;

public class Footer {
    private final SelenideElement twitterLogoLink = $(".social_twitter a");
    private final SelenideElement facebookLogoLink = $(".social_facebook a");
    private final SelenideElement linkedinLogoLink = $(".social_linkedin a");
    private final SelenideElement copyrightText = $(".footer_copy");

    public boolean twitterLogoVisible() {
        return twitterLogoLink.isDisplayed();
    }
    public boolean facebookLogoVisible() {
        return facebookLogoLink.isDisplayed();
    }
    public boolean linkedinLogoVisible() {
        return linkedinLogoLink.isDisplayed();
    }
    public boolean copyrightIsVisible() {
        return copyrightText.isDisplayed();
    }
    @Step("Click the 'Twitter' link")
    public void twitterClick() {
        twitterLogoLink.click();
    }
    @Step("Click the 'Facebook' link")
    public void facebookClick() {
        facebookLogoLink.click();
    }
    @Step("Click the 'linkedIn' link ")
    public void linkedinClick() {
        linkedinLogoLink.click();
    }
    public String copyRightText() {
        return copyrightText.text();
    }



}
