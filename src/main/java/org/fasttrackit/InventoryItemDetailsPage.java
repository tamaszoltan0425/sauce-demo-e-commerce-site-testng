package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class InventoryItemDetailsPage {
    private final SelenideElement itemTitle = $(".inventory_details_name");
    private final SelenideElement itemDescription = $(".inventory_details_desc");
    private final SelenideElement itemPrice = $(".inventory_details_price");
    private final SelenideElement itemAddToCartButton = $(".btn_inventory");


    public String getItemTitle() {
        return itemTitle.text();
    }


    public double getItemPrice() {
        String price = itemPrice.text().substring(1);
        return Double.parseDouble(price);
    }

    public String getItemDescription() {
        return itemDescription.text();
    }

}
