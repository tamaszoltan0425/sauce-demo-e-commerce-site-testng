package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutModal {
    private final SelenideElement firstName = $("#first-name");
    private final SelenideElement lastName = $("#last-name");
    private final SelenideElement zipCode = $("#postal-code");
    private final SelenideElement cancelButton = $("#cancel");
    private final SelenideElement continueButton = $("#continue");
    private final SelenideElement errorMessage = $("h3[data-test='error']");
    private SelenideElement checkOutErrorMessage;
    private static final String NAME_VALIDATION_PATTERN = "^[A-Za-z]{2,20}$";
    private static final String ZIP_VALIDATION_PATTERN = "^\\d{6}$";

    public CheckoutModal() {
    }
    @Step("Click Continue button")
    public void clickContinueCheckout() {
        continueButton.click();
    }
    @Step("Type in first name")
    public void typeInFirstName(String firstn) {
        this.firstName.click();
        firstName.type(firstn);
    }
    public boolean isFirstNameValid() {
        Pattern pattern = Pattern.compile(NAME_VALIDATION_PATTERN);
        System.out.println(this.firstName.getAttribute("value"));
        Matcher matcher = pattern.matcher(this.firstName.getAttribute("value"));
        return matcher.matches();
    }
    @Step("Type in last name")
    public void typeInLastName(String lastn) {
        this.lastName.click();
        this.lastName.type(lastn);
    }
    public boolean isLastNameValid() {
        Pattern pattern = Pattern.compile(NAME_VALIDATION_PATTERN);
        System.out.println(this.lastName.getAttribute("value"));
        Matcher matcher = pattern.matcher(this.lastName.getAttribute("value"));
        return matcher.matches();
    }
    @Step("Type in postal code")
    public void typeInZip(String postal) {
        this.zipCode.click();
        this.zipCode.type(postal);
    }
    public boolean isZipCodeValid() {
        Pattern pattern = Pattern.compile(ZIP_VALIDATION_PATTERN);
        System.out.println(this.zipCode.getAttribute("value"));
        Matcher matcher = pattern.matcher(this.zipCode.getAttribute("value"));
        return matcher.matches();
    }
    @Step("Click Cancel button")
    public void clickCancel() {
        cancelButton.click();
    }
    public boolean isCheckoutErrorMessageVisible() {
        return errorMessage.isDisplayed();
    }
    public String getErrorMessage() {
        return this.errorMessage.text();
    }
    public boolean isCheckOutRegExErrorMessVisible() {
        return checkOutErrorMessage.isDisplayed();
    }

}
