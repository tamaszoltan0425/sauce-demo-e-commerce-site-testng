package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Cart {
    private final SelenideElement cartPageTitle = $(".title");
    private final SelenideElement continueShoppingButton = $("#continue-shopping");
    private final SelenideElement checkoutButton = $(".checkout_button");
    private final SelenideElement cartItem = $(".cart_item");

    public int getCartSize() {
        return cartItems.size();
    }

    private final ElementsCollection cartItems = $$(".cart_item");

    public String getCartPageTitle() {
        return cartPageTitle.getText();
    }

    @Step("Click on the Continue Shopping button")
    public void clickContinueShopping() {
        continueShoppingButton.click();
    }

    @Step("Click on the Checkout button")
    public void clickCheckout() {
        checkoutButton.click();
    }

    public boolean cartIsEmpty() {
        return !cartItem.isDisplayed();
    }

    public boolean checkoutButtonIsClickable() {
        return checkoutButton.isEnabled();
    }


}
