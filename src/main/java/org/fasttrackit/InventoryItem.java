package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Selenide.$;

public class InventoryItem {

    private final SelenideElement listingLink;
    private final SelenideElement addToCartButton;
    private final String listingTitle;
    private final SelenideElement buttonText;
    private final SelenideElement listingPrice;


    public InventoryItem(String listingID) {

        String listingIDSelector = String.format("#item_%s_title_link", listingID);
        this.listingLink = $(listingIDSelector);
        this.listingTitle = listingLink.text();
        this.addToCartButton = listingLink.parent().sibling(0).$(".btn_inventory");
        this.buttonText = listingLink.parent().sibling(0).$(".btn_inventory");
        this.listingPrice = listingLink.parent().sibling(0).$("div");

    }

    public String getListingTitle() {
        return listingTitle;
    }

    @Step("Click on the 'Add to Cart' button")
    public void clickAddToCartButton() {
        addToCartButton.click();
    }

    public void clickRemoveFromCartButton() {
        addToCartButton.click();
    }

    public String getButtonText() {
        return buttonText.text();
    }

    public boolean addToCartButtonClickable() {
        return this.addToCartButton.is(enabled);
    }
    @Step("Click on the listing title")
    public void clickListingLink() {
        this.listingLink.click();
    }
    public double listingPrice() {
        String priceString = listingPrice.text().substring(1);
        return Double.parseDouble(priceString);
    }
}
