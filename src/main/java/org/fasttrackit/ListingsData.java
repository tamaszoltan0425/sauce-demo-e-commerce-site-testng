package org.fasttrackit;

public class ListingsData {
    private final String id;
    private final String title;

    public ListingsData(String id, String title) {
        this.id = id;
        this.title = title;
    }
    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return this.title;
    }
}
