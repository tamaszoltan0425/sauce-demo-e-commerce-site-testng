package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Header {
    private final SelenideElement shoppingCartBadge = $(".shopping_cart_link");
    private final SelenideElement burgerMenuButton = $("#react-burger-menu-btn");
    private final SelenideElement appLogo = $(".app_logo");




    public boolean cartIconIsDisplayed() {
        return shoppingCartBadge.isDisplayed();
    }
    public boolean siteNameIsDisplayed() {
        return appLogo.isDisplayed();
    };

    public String getCartBadgeValue() {
        return this.shoppingCartBadge.text();
    }
    @Step("Click on the Cart icon")
    public void clickOnTheCart() {
        shoppingCartBadge.click();
    }
    @Step("Click on the burger menu button")
    public void openPageMenu() {
        burgerMenuButton.click();
    }
    public boolean burgerMenuButtonIsDisplayed() {
        return burgerMenuButton.isDisplayed();
    }
}
