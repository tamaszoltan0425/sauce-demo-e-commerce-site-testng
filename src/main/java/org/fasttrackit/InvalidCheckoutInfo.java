package org.fasttrackit;

public class InvalidCheckoutInfo extends CheckoutInfo {
    private final String checkOutErrorMessage;
    public InvalidCheckoutInfo(String firstName, String lastName, String zipCode, String checkOutErrorMessage) {
        super(firstName, lastName, zipCode);
        this.checkOutErrorMessage = checkOutErrorMessage;
    }
    public String getCheckOutErrorMessage() {
        return checkOutErrorMessage;
    }

    @Override
    public String toString() {
        return checkOutErrorMessage;
    }
}
