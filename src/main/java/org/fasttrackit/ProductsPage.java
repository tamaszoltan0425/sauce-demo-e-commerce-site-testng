package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.google.common.collect.Ordering;
import io.qameta.allure.Step;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.*;

public class ProductsPage {

    public static final String PRODUCTSURL = "https://www.saucedemo.com/inventory.html";
    private final SelenideElement productPageTitle = $(".title");

    private final SelenideElement logOutButton = $("#logout_sidebar_link");
    private final SelenideElement sortByContainer = $(".select_container");
    private final SelenideElement sortByAtoZ = $("[value='az']");
    private final SelenideElement sortByZtoA = $("[value='za']");
    private final SelenideElement sortLotoHi = $("[value='lohi']");
    private final SelenideElement sortHitoLo = $("[value='hilo']");
    private final SelenideElement resetButton = $("#reset_sidebar_link");
    private final ElementsCollection inventoryItems = $$(".inventory_item");
    private final ElementsCollection inventoryItemNames = $$(".inventory_item_name ");
    private final ElementsCollection inventoryPricesSE = $$(".inventory_item_price");

    @Step("Opening Products page")
    public void openProductsPage() {
        open(PRODUCTSURL);
    }
    public String getProductPageTitle() {
        return productPageTitle.text();
    }

    public boolean sortByContainerisDisplayed() {
        return sortByContainer.isDisplayed();
    }
    public void clickOnSortByContainer() {
        sortByContainer.click();
    }
    @Step("Click to sort alphabetically increasing")
    public void setSortByAtoZ() {
        sortByAtoZ.click();
    }
    @Step("Click to sort alphabetically decreasing")
    public void setSortByZtoA() {
        sortByZtoA.click();
    }
    public boolean isSetSortByZtoAClickable() {
        return sortByZtoA.isEnabled();
    }
    @Step("Click to sort by price increasing")
    public void setSortLotoHi() {
        sortLotoHi.click();
    }
    @Step("Click to sort by price decreasing")
    public void setSortHitoLo() {
        sortHitoLo.click();
    }
    @Step("Click Logout button")
    public void logOutCurrentUser() {
        logOutButton.click();
    }
    @Step("Click 'Reset' button")
    public void clickToResetState(){
        resetButton.click();
    }
    public int getNumberOfElements() {
        return inventoryItems.size();
    }
    public List<String> inventoryItemTitles() {
        return inventoryItemNames.texts();
    }
    public List<String> inventoryItemsPrices() {
        return inventoryPricesSE.texts();
    }

    public boolean inventoryAtoZ() {
        List<String> inventoryItemsNames = new ArrayList<>();
        for (int i = 0; i < inventoryItemTitles().size(); i++) {
            inventoryItemsNames.add(inventoryItemTitles().get(i));
        }
        System.out.println(inventoryItemsNames);
        return Ordering.natural().isOrdered(inventoryItemsNames);
    }
    public boolean inventoryZtoA() {
        List<String> inventoryItemsNames = new ArrayList<>();
        for (int i = 0; i < inventoryItemTitles().size(); i++) {
            inventoryItemsNames.add(inventoryItemTitles().get(i));
        }
        System.out.println(inventoryItemsNames);
        return Ordering.natural().reverse().isOrdered(inventoryItemsNames);
    }
    public boolean inventoryPricesAscending() {
        List<String> inventoryPriceSubString = new ArrayList<>();
        for (int i = 0; i < inventoryItemsPrices().size(); i++) {
            inventoryPriceSubString.add(inventoryItemsPrices().get(i).substring(1));
        }
        List<Double> price = new ArrayList<>();
        for (String s : inventoryPriceSubString) {
            price.add(Double.valueOf(s));
        }
        return Ordering.natural().isOrdered(price);
    }
    public boolean inventoryPricesDescending() {
        List<String> inventoryPriceSubString = new ArrayList<>();
        for (int i = 0; i < inventoryItemsPrices().size(); i++) {
            inventoryPriceSubString.add(inventoryItemsPrices().get(i).substring(1));
        }
        List<Double> price = new ArrayList<>();
        for (String s : inventoryPriceSubString) {
            price.add(Double.valueOf(s));
        }
        return Ordering.natural().reverse().isOrdered(price);
    }



}
