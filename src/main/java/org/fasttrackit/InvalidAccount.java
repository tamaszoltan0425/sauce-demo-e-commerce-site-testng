package org.fasttrackit;

public class InvalidAccount extends Account {


    private final String errorMessage;

    public InvalidAccount(String user, String password, String errorMessage) {
        super(user, password);
        this.errorMessage = errorMessage;
    }
    public String getErrorMessage() {
        return errorMessage;
    }


}
