package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutOverview {
    private final SelenideElement cartItemRow;
    private final String cartItemPrice;
    private final SelenideElement itemsSubTotal = $(".summary_subtotal_label");
    private final SelenideElement itemsTax = $(".summary_tax_label");
    private final SelenideElement priceTotal = $(".summary_total_label");
    private final SelenideElement finishButton = $("#finish");
    private final SelenideElement successMessage = $(".complete-header");

    public CheckoutOverview(String cartItemID) {
        String cartItembyID = String.format("item_%s_title_link", cartItemID);
        SelenideElement shoppingCartItem = $(By.id(cartItembyID));
        this.cartItemRow = shoppingCartItem.parent().parent();
        this.cartItemPrice = cartItemRow.$(".inventory_item_price").text();
    }
    public double getCartItemPrice() {
        return Double.parseDouble(this.cartItemPrice.substring(cartItemPrice.indexOf("$") +1));
    }
    public double getItemsSubTotal() {
        return Double.parseDouble(itemsSubTotal.text().substring(itemsSubTotal.text().indexOf("$") + 1));
    }
    public double getItemsTax() {
        return Double.parseDouble(itemsTax.text().substring(itemsTax.text().indexOf("$") + 1));
    }
    public double getTotalPrice() {
        return Double.parseDouble(priceTotal.text().substring(priceTotal.text().indexOf("$") + 1));
    }
    @Step("Click finish button")
    public void clickFinish() {
        finishButton.click();
    }
    public String getSuccessMessage() {
        return successMessage.text();
    }





}
