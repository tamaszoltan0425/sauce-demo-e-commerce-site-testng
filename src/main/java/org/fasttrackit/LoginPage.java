package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class LoginPage {

    public static final String URL = "https://www.saucedemo.com/";
    private final SelenideElement loginPageTitle = $(".login_logo");
    private final SelenideElement userName = $(".form_group #user-name");
    private final SelenideElement password = $(".form_group #password");
    private final SelenideElement loginButton = $("#login-button");
    private final SelenideElement errorMessage = $(".error-message-container");


    public void openLoginPage() {
        open(URL);
    }

    public String getLoginPageTitle() {
        return loginPageTitle.text();
    }

    @Step("Type in username")
    public void typeUserName(String user) {
        userName.click();
        userName.type(user);
    }
    @Step("Type in password")
    public void typePassword(String pass) {
        password.click();
        password.type(pass);
    }
    @Step("Click Login button")
    public void clickLoginButton() {
        loginButton.click();
    }

    public String getErrorMessage() {
        return errorMessage.text();
    }
    public boolean isErrorMessageVisible() {
        return errorMessage.isDisplayed();
    }
    @Step("Hit 'Enter' key")
    public void enterKeyHit() {
        loginButton.pressEnter();
    }



}
