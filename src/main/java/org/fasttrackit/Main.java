package org.fasttrackit;


public class Main {
    public static void main(String[] args) {
        System.out.println("Standard user can successfully login with credentials....");
        LoginPage loginPage = new LoginPage();
        loginPage.openLoginPage();
        loginPage.getLoginPageTitle();
        loginPage.typeUserName("standard_user");
        loginPage.typePassword("secret_sauce");
        loginPage.clickLoginButton();

        ProductsPage productsPage = new ProductsPage();
        productsPage.openProductsPage();
        InventoryItem inventoryItem = new InventoryItem("4");
        inventoryItem.clickAddToCartButton();
        Header header = new Header();
        header.clickOnTheCart();
        Cart cart = new Cart();
        cart.clickCheckout();
        CheckoutModal checkoutModal = new CheckoutModal();
//        checkoutModal.typeInFirstName(validCheckoutInfo.getFirstName());
        checkoutModal.isFirstNameValid();


//        productsPage.getProductPageTitle();
//        int numberOfElements = productsPage.getNumberOfElements();
//        System.out.println(numberOfElements);
//
//        System.out.println("Standard user can add product to cart");
//        productsPage.openProductsPage();
//        InventoryItem inventoryItem = new InventoryItem("5");
//        System.out.println(inventoryItem.getListingTitle());
////        System.out.println(inventoryItem.getButtonText());
//        inventoryItem.clickAddToCartButton();
//        inventoryItem = new InventoryItem("1");
//        inventoryItem.clickAddToCartButton();
//
//        sleep(3*1000);
//        inventoryItem = new InventoryItem("5");
//        System.out.println(inventoryItem.getButtonText());
//        System.out.println("Standard user can retrieve cart items");
//        Header header = new Header();
//        header.clickOnTheCart();
//        List<CartItem> cartItemList = new ArrayList<>();
//        Cart cart = new Cart();
//        int cartSize = cart.getCartSize();
//        for (int i = 0; i <= cartSize - 1; i++) {
//            String s = String.valueOf((i+3));
//            cartItemList.add(new CartItem(s));
//        }
//        for (CartItem cartItem : cartItemList) {
//            System.out.println(cartItem.getCartItemName());
//        }


//        System.out.println("Standard user can access Cart page");
//        System.out.println("Click Cart link in Header");
//        System.out.println("'Your cart' page title is displayed.");
//        productsPage.openProductsPage();
//        Header header = new Header();
//        header.clickOnTheCart();
//        Cart cart = new Cart();
//        cart.getCartPageTitle();
//        System.out.println(cart.getCartPageTitle());
////        System.out.println(cart.cartIsEmpty());
//        System.out.println("Standard user can sort products alphabetically decreasing");
//        System.out.println(productsPage.inventoryItemTitles());
//        System.out.println(productsPage.inventoryItemTitles().get(0));
//        int listLength = productsPage.inventoryItemTitles().size();
//        System.out.println(productsPage.inventoryItemTitles().get(listLength-1));
//        System.out.println();
//        productsPage.clickOnSortByContainer();
//        productsPage.setSortByZtoA();
//        System.out.println(productsPage.inventoryItemTitles().get(0));
//        System.out.println(productsPage.inventoryItemTitles().get(listLength-1));
//        System.out.println(productsPage.inventoryItemsPrices());
//        System.out.println(productsPage.inventoryPricesAscending());
//        productsPage.clickOnSortByContainer();
//        productsPage.setSortByZtoA();
//        productsPage.inventoryAtoZ();
//        System.out.println(productsPage.inventoryAtoZ());


    }
}