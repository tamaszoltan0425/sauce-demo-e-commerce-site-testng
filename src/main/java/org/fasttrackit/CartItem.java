package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;

public class CartItem {
    private final SelenideElement cartItemRow;
    private final String cartItemTitle;
    private final String cartItemQuantityDisplayed;
    private final SelenideElement itemQuantityIncDecr;
    private final String cartItemPrice;
    private final SelenideElement removeButton;


    public CartItem(String cartItemID) {
        String cartItembyID = String.format("item_%s_title_link", cartItemID);
        SelenideElement shoppingCartItem = $(By.id(cartItembyID));
        this.cartItemRow = shoppingCartItem.parent().parent();
        this.cartItemTitle = cartItemRow.$(".inventory_item_name").text();
        this.cartItemQuantityDisplayed = cartItemRow.$(".cart_quantity").text();
        this.itemQuantityIncDecr = cartItemRow.$(".cart_quantity");
        this.cartItemPrice = cartItemRow.$(".inventory_item_price").text();
        this.removeButton = cartItemRow.$(".cart_button");

    }

    public String getCartItemTitle() {
        return cartItemTitle;
    }

    @Step("Click the Remove Button")
    public void clickToRemoveItem() {
        this.removeButton.click();
    }


}
